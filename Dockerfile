FROM python:3.9.5

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client stress-ng\
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . .

EXPOSE 80
ENTRYPOINT ["python3", "manage.py", "runserver", "0.0.0.0:80"]
