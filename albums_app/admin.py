from django.contrib import admin
from albums_app.models import Album

admin.site.register(Album)