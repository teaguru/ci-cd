from django.db import models
from django.urls import reverse

#kind, collectionName, trackName, collectionPrice, trackPrice, primaryGenreName, trackCount, trackNumber, releaseDate.
#album_name.collection_name, album_name.primaryGenreName, album_name.track_count, album_name.release_date, album_name.collection_id)
class Album(models.Model):
    collectionPrice = models.DecimalField(max_digits=9, decimal_places=2)
    primaryGenreName = models.CharField(max_length=200, default="Pop")
    trackCount = models.IntegerField(default=1)
    name = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    releaseDate = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('albums_app:album_edit', kwargs={'pk': self.pk})

    class Meta:
        ordering = ["releaseDate"]    