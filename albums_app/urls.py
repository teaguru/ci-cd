from django.urls import path

from . import views

app_name = 'albums_app'

urlpatterns = [
  path('', views.AlbumList.as_view(), name='album_list'),
  path('new/', views.BookCreate.as_view(), name='album_new'),
  path('edit/<int:pk>/', views.BookUpdate.as_view(), name='album_edit'),
  path('delete/<int:pk>/', views.BookDelete.as_view(), name='album_delete'),
  path('itunes/', views.itunes_renew, name='test'),
  path('stress/', views.stress_ng, name='stress'),
  path('async/', views.itunes_async, name='async')
  #path('renew/', views.ItunesUpdate.as_view(), name='album_renew'),
]
