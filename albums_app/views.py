import datetime
import time
import subprocess
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.urls import reverse_lazy
import itunespy, json, subprocess, os
from django.core.management import execute_from_command_line

from .models import Album

class AlbumList(ListView):
    model = Album
    fields = ['name', 'author']

class BookCreate(CreateView):
    model = Album
    fields = ['name', 'author', 'collectionPrice', 'primaryGenreName', 'trackCount', 'releaseDate']
    success_url = reverse_lazy('albums_app:album_list')

class BookUpdate(UpdateView):
    model = Album
    fields = ['name',  'author']
    success_url = reverse_lazy('albums_app:album_list')

class BookDelete(DeleteView):
    model = Album
    success_url = reverse_lazy('albums_app:album_list')


def itunes_renew(request):
        artist = itunespy.search_artist('The Beatles')  # Returns a list
        albums = artist[0].get_albums()  # Get albums from the first result
        album_array = []
        id = 0
        for album in albums:
            time.sleep(1)
            album_name = album
            album = itunespy.lookup(album.collection_id)
            tracks = album[0].get_tracks()  # Get tracks from the first result
            album_dict = dict()
            if tracks == []:
                pass
            else:
                id = id + 1
                print("---------------------------------------")
                print("collectin name^^^^")
                # release_date = datetime.strptime(album_name.release_date, "%Y-%m-%d") 
                print(album_name.collection_name, album_name.primaryGenreName, album_name.track_count,
                      album_name.release_date, album_name.collection_id)
                album_dict = {"model": "albums_app.Album",
                              "pk": id,
                              "fields": {
                                  "collectionPrice": album_name.collection_price,
                                  "primaryGenreName": album_name.primaryGenreName,
                                  "trackCount": album_name.track_count,
                                  "name": album_name.collection_name,
                                  "author": album_name.artist_name,
                                  "releaseDate": datetime.datetime.strptime(album_name.release_date,"%Y-%m-%dT%H:%M:%SZ").strftime("%Y-%m-%d")
                                  
                              }
                              }
                album_array.append(album_dict)
                for track in tracks:
                    print(str(
                        track.track_number) + " " + track.kind + " " + track.collection_name + ': ' + track.track_name + " price " + str(
                        track.track_price) + "time: " + str(track.get_track_time_minutes()))
                print('Total playing time: ' + str(album[0].get_album_time()))

        print(album_array)
        
        album_array = json.dumps(album_array, separators=(',', ':'))
         
        album_array = str(album_array)
        
        f = open('/usr/src/app/albums_app/fixtures/data.json', 'w')
        f.write(album_array)
        f.close()
        return HttpResponse('OK', status=200)

def itunes_async(request):
    execute_from_command_line(['manage.py', "loaddata", '/usr/src/app/albums_app/fixtures/data.json'])
    return HttpResponse('OK', status=200)

def stress_ng(request):
    print('stress test')
    subprocess.run(["stress-ng", "--cpu", "4", "--cpu-method", "matrixprod", "--metrics", "--timeout", "60"])
    return HttpResponse('OK', status=200)

