from django.urls import include, path
from django.contrib import admin

import theme.views

urlpatterns = [
    path('',  include('albums_app.urls')),
     # Enable built-in authentication views

    # Enable built-in admin interface
    path('admin/', admin.site.urls)
]
